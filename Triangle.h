//
//  Triangle.hpp
//  OpenGLTriangles
//
//  Created by Krzysztof Juchnowicz and Pawel Wroblewski on 17.05.2018.
//  Copyright © 2018 Krzysztof Juchnowicz & Pawel Wroblewski. All rights reserved.
//

#ifndef Triangle_hpp
#define Triangle_hpp

#include <stdio.h>

class Triangle {

    
public:
	int firstX;
	int firstY;
	int secondX;
	int secondY;
	int thirdX;
	int thirdY;
	int colorCode;
    
	Triangle();
	Triangle(int, int, int, int, int, int, int);
	float* getCentroid();
};

#endif /* Triangle_hpp */
