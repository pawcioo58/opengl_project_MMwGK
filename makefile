mainApp.out: main.o triangle.o
	g++ triangle.o main.o -o main.out -lGL -lGLU -lglut
	./main.out # run application

main.o: main.cpp
	g++ -c main.cpp -o main.o

triangle.o: Triangle.cpp Triangle.h
	g++ -c Triangle.cpp -o triangle.o

clean :
	rm -f  *.o
