//
//  main.cpp
//  OpenGLTriangles
//
//  Created by Krzysztof Juchnowicz and Pawel Wroblewski on 17.05.2018.
//  Copyright © 2018 Krzysztof Juchnowicz & Pawel Wroblewski. All rights reserved.
//
//  specification: http://lux.dmcs.p.lodz.pl/~mariuszz/TGK.pdf

#include <string>
#include <sstream>
#include <iostream>
#include <math.h>	/* sqrt */
#include <stdlib.h>
#include <GL/glut.h>
#include "Triangle.h"

using namespace std;

// configuration
int SCREEN_FPS = 25;
int ROTATION_SPEED = 3;
int ROTATIONS_PER_SPREADOUT = 10;

// variables
Triangle *triangles[24];
float rotateMainLeft = 0;
float rotateLocalRight = 0;
float spreadOutDistance = 0;

void changeColor(int colorCode){
	switch(colorCode){
		case 1:
			//blue color
			glColor3f(0, 0, 1);
			break;
		case 2:
			//green color
			glColor3f(0, 1, 0);
			break;
		case 3:
			//orange color
			glColor3f(1.0, 0.5, 0);
			break;
		case 4:
			//red color
			glColor3f(1.0, 0, 0);
			break;
		case 5:
			//yellow color
			glColor3f(1.0, 1.0, 0);
			break;
		case 6:
			//purple color
			glColor3f(1.0, 0, 1.0);
			break;
		default:
			glColor3f(0, 0, 0);
			break;
	}
}

float* spreadOutForPoint(float triangleCentroid[]){
	float *result = new float[2];
	float d = sqrt(triangleCentroid[0]*triangleCentroid[0] +
			triangleCentroid[1]*triangleCentroid[1]); // length of vector

	result[0] = ((d+spreadOutDistance)/d)*triangleCentroid[0]; // calculate x for new point 
	result[1] = ((d+spreadOutDistance)/d)*triangleCentroid[1]; // calculate y for new point

	// store only shift from centroid
	result[0] = result[0]-triangleCentroid[0]; 
	result[1] = result[1]-triangleCentroid[1];
	
	return result;
}

void drawScene() {
	// Clear displayed screen
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	//Sets object drawing perspective
	glMatrixMode(GL_MODELVIEW);
	//Resets drawing perspective
	glLoadIdentity();
	// Camera go back in order to see whole area
	glTranslatef(0.0f, 0.0f, -15.0f);
	
	//Draw objects
	for (int i = 0; i < 24; i++) {

		glPushMatrix();	// create matrix

		glRotatef(rotateMainLeft,0,0,1); // rotation whole world
		
		float* triangleCentroid = triangles[i]->getCentroid(); // get centroid of current triangle
		float* translateFromCenter = spreadOutForPoint(triangleCentroid); // calculate spreadOut for current triangle

		glTranslatef(translateFromCenter[0],translateFromCenter[1],0); // applay spreadOut transition

		// rotation about the centroid 
		glTranslatef(triangleCentroid[0],triangleCentroid[1],0); // jump to centroid
 		glRotatef(rotateLocalRight,0,0,1);	// rotation
		glTranslatef(-triangleCentroid[0],-triangleCentroid[1],0); //revert previous translation
	
		changeColor(triangles[i]->colorCode); // set color for current triangle
		
		// drawing triangle
		glBegin(GL_TRIANGLES);
		glVertex3f(triangles[i]->firstX, triangles[i]->firstY, 0.0);
		glVertex3f(triangles[i]->secondX, triangles[i]->secondY, 0.0);
		glVertex3f(triangles[i]->thirdX, triangles[i]->thirdY, 0.0);
		glEnd();

		glPopMatrix();
	}
	
	//Sets 3d scene for screen
	glutSwapBuffers();
}


void handleResize(int w, int h) {
	//Set OpenGL initial frame
	glViewport(0, 0, w, h);
	//Enables perspective camera
	glMatrixMode(GL_PROJECTION);
	//Resets camera
	glLoadIdentity();
	//Sets perspective
	gluPerspective(45.0,					// Camera angle
				   (double)w / (double)h,	// Screen ratio
				   1.0,						// Perspective z start
				   200.0);					// Perspective z end
}

void handleUpdate(int parameter) {
	rotateMainLeft=(rotateMainLeft + ROTATION_SPEED); // new value of left rotation

	if(rotateMainLeft>=360*ROTATIONS_PER_SPREADOUT*2){	// reset value of angle
		rotateMainLeft=0;	
	}

	rotateLocalRight=-(rotateMainLeft*4); // calculate angle rotation about the centroid 3x (1 do przeciw wagi main rotate + 3 per obrot)
	
	spreadOutDistance = (rotateMainLeft/(360.0*ROTATIONS_PER_SPREADOUT)); // spread out distance from 0 to 1 during 10 rotates

	// decreasing spread out distance
	if(rotateMainLeft>(360.0*ROTATIONS_PER_SPREADOUT))
		spreadOutDistance = 2-spreadOutDistance; 

	if(((int)rotateMainLeft)%5==0) // display spreadOut value
		std::cout<< " spreadOutDistance: " <<spreadOutDistance<<"\n";

 	glutPostRedisplay();
	glutTimerFunc(1000/SCREEN_FPS, handleUpdate, 0); // setting next timer
}

void initTrainglesArray(){
	triangles[0] = new Triangle(0,1,0,0,1,0,1);
	triangles[1] = new Triangle(0,-1,0,0,1,0,1);
	triangles[2] = new Triangle(-1,0,0,0,0,-1,1);
	triangles[3] = new Triangle(-1,0,0,0,0,1,1);
	
	triangles[4] = new Triangle(0,2,0,1,1,1,2);
	triangles[5] = new Triangle(1,1,1,0,2,0,3);
	triangles[6] = new Triangle(2,0,1,0,1,-1,2);
	triangles[7] = new Triangle(1,-1,0,-1,0,-2,3);
	triangles[8] = new Triangle(-1,-1,0,-1,0,-2,2);
	triangles[9] = new Triangle(-2,0,-1,0,-1,-1,3);
	triangles[10] = new Triangle(-1,1,-1,0,-2,0,2);
	triangles[11] = new Triangle(0,2,0,1,-1,1,3);
	
	triangles[12] = new Triangle(0,3,0,2,1,2,4);
	triangles[13] = new Triangle(1,2,1,1,2,1,5);
	triangles[14] = new Triangle(2,1,2,0,3,0,6);
	
	triangles[15] = new Triangle(2,-1,2,0,3,0,4);
	triangles[16] = new Triangle(1,-2,1,-1,2,-1,5);
	triangles[17] = new Triangle(0,-3,0,-2,1,-2,6);
	
	triangles[18] = new Triangle(0,-3,0,-2,-1,-2,4);
	triangles[19] = new Triangle(-1,-2,-1,-1,-2,-1,5);
	triangles[20] = new Triangle(-2,-1,-2,0,-3,0,6);
	
	triangles[21] = new Triangle(0,3,0,2,-1,2,6);
	triangles[22] = new Triangle(-1,2,-1,1,-2,1,5);
	triangles[23] = new Triangle(-2,1,-2,0,-3,0,4);
}

int main(int argc, char** argv) {
	initTrainglesArray();
	//Initialize GLUT
	glutInit(&argc, argv);
	// Set GLUT Display mode
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
	// Set the window size
	glutInitWindowSize(1024, 1024);
	//Create the window
	glutCreateWindow("Triangles OpenGL");
	//Initialize depth buffer - shows objects according to distance not drawing order
	glEnable(GL_DEPTH_TEST);
	// Handle for screen redraw
	glutDisplayFunc(drawScene);
	// Handler for window resize
	glutReshapeFunc(handleResize);
	// Runs update method after given interval
	glutTimerFunc(1000/SCREEN_FPS, handleUpdate, 0);
	// Starts glut main loop
	glutMainLoop();
	
	return 0;
}
