//
//  Triangle.cpp
//  OpenGLTriangles
//
//  Created by Krzysztof Juchnowicz and Pawel Wroblewski on 17.05.2018.
//  Copyright © 2018 Krzysztof Juchnowicz & Pawel Wroblewski. All rights reserved.
//

#include "Triangle.h"

Triangle::Triangle(int firstX, int firstY, int secondX, int secondY, int thirdX, int thirdY, int colorCode){
    this->firstX = firstX;
    this->firstY = firstY;
    this->secondX = secondX;
    this->secondY = secondY;
    this->thirdX = thirdX;
    this->thirdY = thirdY;
    this->colorCode = colorCode;
}

Triangle::Triangle(){
    this->firstX = 0;
    this->firstY = 0;
    this->secondX = 0;
    this->secondY = 0;
    this->thirdX = 0;
    this->thirdY = 0;
}

float* Triangle::getCentroid(){
	float* result = new float[2];
	result[0] = (float)(secondX+((2/3.0)*(((thirdX+firstX)/2.0)-secondX))); // centroid for x
	result[1] = (float)(secondY+((2/3.0)*(((thirdY+firstY)/2.0)-secondY))); // centroid for y
	return result;
}
